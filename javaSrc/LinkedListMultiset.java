import java.io.PrintStream;
import java.util.*;

public class LinkedListMultiset<T> extends Multiset<T>
{
	protected LinkedListNode<T> mHead;
	protected int mLength;
	
	
	public LinkedListMultiset() {
		mHead = null;
	} // end of LinkedListMultiset()
	
	
	public void add(T item) {
		
		//step through list, attempting to find item
		LinkedListNode<T> currentNode = mHead;
		boolean found = false;
		
		//step through list until item is found, update item count.
		while(!found && currentNode != null){
			if((currentNode.getElement()).equals(item)){
				currentNode.setAmount(currentNode.getAmount() + 1);
				found = true;
			}
			else {
				currentNode = currentNode.getNext();
			}
		}
		
		//else create new node for item, and add it to list
		if(!found) {
			LinkedListNode<T> newNode = new LinkedListNode<T>(item);
			newNode.setNext(mHead);
			mHead = newNode;
		}	
		
	} // end of add()
	
	// Returns amount of items stored in list
	public int search(T item) {
		LinkedListNode<T> currentNode = mHead;
		
		while(currentNode != null){
			if((currentNode.getElement()).equals(item)){
				return currentNode.getAmount();
			}
			else {
				currentNode = currentNode.getNext();
			}
		}		
		return 0;
	} // end of add()
	
	
	public void removeOne(T item) {
		LinkedListNode<T> currentNode = mHead;
		LinkedListNode<T> previousNode = null;
		boolean found = false;
		
		while(currentNode != null && !found){
			if(currentNode.getElement().equals(item)){
				found = true;
				
				if(currentNode.getAmount() > 1){
					currentNode.setAmount(currentNode.getAmount()-1);
				}
				else { //only 1 left of an item, therefore node needs to be removed
					if(currentNode != mHead){
						previousNode.setNext(currentNode.getNext());
					}
					else { //currentNode == mHead
						mHead = currentNode.getNext();
					}
				}
			}
			else {
				previousNode = currentNode;
				currentNode = currentNode.getNext();
			}
		}
	} // end of removeOne()
	
	
	public void removeAll(T item) {
		LinkedListNode<T> previousNode = null;
		LinkedListNode<T> currentNode = mHead;
		boolean found = false;
		
		while(currentNode != null && !found){
			if(currentNode.getElement().equals(item)){
				found = true;
				if(currentNode != mHead){
					previousNode.setNext(currentNode.getNext());
				}
				else { //currentNode == mHead
					mHead = currentNode.getNext();
				}
			}
			else{
				previousNode = currentNode;
				currentNode = currentNode.getNext();
			}
		}
	} // end of removeAll()
	
	
	public void print(PrintStream out) {
		
		LinkedListNode<T> currentNode = mHead;
		while(currentNode != null){
			
			out.println(currentNode.getElement() + printDelim + currentNode.getAmount());
			currentNode = currentNode.getNext();
		}
	} // end of print()
	
	
} // end of class LinkedListMultiset