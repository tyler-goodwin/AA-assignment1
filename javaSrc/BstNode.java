/*ele
*	BstNode.java
*
*	node for storing data in Binary Search Trees
*
*
*/

public class BstNode<T>
{	
	protected T element;
	protected int amount;
	protected BstNode left;
	protected BstNode right;
	
	public BstNode(T item){
		element = item;
		amount = 1;
		right = null;
		left = null;
	}
	
	public T getElement(){
		return element;
	}
	
	public int getAmount(){
		return amount;
	}
	
	public BstNode getLeft(){
		return left;
	}
	
	public BstNode getRight(){
		return right;
	}
	
	public void setLeft(BstNode new_left){
		left = new_left;
	}
	
	public void setRight(BstNode new_right){
		right = new_right;
	}
	
	public void setAmount(int new_amount){
		amount = new_amount;
	}
	
	
}