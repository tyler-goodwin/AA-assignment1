import java.io.*;
import java.util.Random;

public class Generator
{
	
	public static void main(String[] args)
	{
		try{
		//	PrintWriter writer = new PrintWriter("out.txt", "UTF-8");
		
		Random rn = new Random();
		String arg = args[0];
		int random;
		
		if(arg == "s")
		{
			//100 adds, 50 unique choices.
			PrintWriter writer = new PrintWriter("Add_s_100.in", "UTF-8");
			for (int j = 0; j < 100; j++)
			{
				random = rn.nextInt(51) + 1;
				writer.println("A " + random);
			}
			writer.close();
			
			//1000 adds
			PrintWriter writer1 = new PrintWriter("Add_s_1000.in", "UTF-8");
			for (int j = 0; j < 1000; j++)
			{
				random = rn.nextInt(51) + 1;
				writer1.println("A " + random);
			}
			writer1.close();
			
			//10000 adds
			PrintWriter writer2 = new PrintWriter("Add_s_10000.in", "UTF-8");
			for (int j = 0; j < 10000; j++)
			{
				random = rn.nextInt(51) + 1;
				writer2.println("A " + random);
			}
			writer.close();
		}
		
		else if (arg == "m")
		{
			
			PrintWriter writer3 = new PrintWriter("Add_m_100", "UTF-8");
			for (int j = 0; j < 100; j++)
			{
				random = rn.nextInt(501) + 1;
				writer3.println("A " + random);
			}
			writer3.close();
			
			//1000 adds
			PrintWriter writer4 = new PrintWriter("Add_m_1000", "UTF-8");
			for (int j = 0; j < 1000; j++)
			{
				random = rn.nextInt(501) + 1;
				writer4.println("A " + random);
			}
			writer4.close();
			
			PrintWriter writer5 = new PrintWriter("Add_m_10000", "UTF-8");
			for (int j = 0; j < 10000; j++)
			{
				random = rn.nextInt(501) + 1;
				writer5.println("A " + random);
			}
			writer5.close();
		}
		
		else if (args[0] == arg)
		{
			PrintWriter writer6 = new PrintWriter("Add_l_100", "UTF-8");
			for (int j = 0; j < 100; j++)
			{
				random = rn.nextInt(1001) + 1;
				writer6.println("A " + random);
			}
			writer6.close();
			
			//1000 adds
			PrintWriter writer7 = new PrintWriter("Add_l_1000", "UTF-8");
			for (int j = 0; j < 1000; j++)
			{
				random = rn.nextInt(1001) + 1;
				writer7.println("A " + random);
			}
			writer7.close();
			
			PrintWriter writer8 = new PrintWriter("Add_l_10000", "UTF-8");
			for (int j = 0; j < 10000; j++)
			{
				random = rn.nextInt(1001) + 1;
				writer8.println("A " + random);
			}
			writer8.close();
		}
		else
			System.out.println(args[0]);
		
	}
		catch(Exception e)
		{
			
		}
	
	}
	
}